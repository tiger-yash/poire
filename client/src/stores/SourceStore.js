import { makeObservable, observable, action, reaction } from 'mobx'
import SourceNode from '@models/SourceNode'
import NodeAPI from '@api/NodeAPI'

/** Stores all sources */
class SourceStore {
  /** @property {api.NodeApi} nodeApi - Create requests for the SATIE renderer */
  nodeAPI = null

  /** @property {Map<string, SourceNode>}  - Array of sources */
  sources = new Map()

  /** @property {Map<string, Map<string, string|boolean|number>>}  - Array of sources */
  parameters = new Map()

  /** @property {SourceNode} - Selected source */
  selectedNodeName = null

  get selectedSource () {
    let selectedSource = null

    if (this.selectedNodeName) {
      selectedSource = SourceNode.fromMap(
        this.selectedNodeName,
        this.parameters.get(this.selectedNodeName)
      )
    }

    return selectedSource
  }

  nodeAlreadyExist (nodeName) {
    return this.parameters.has(nodeName)
  }

  constructor (socketStore) {
    makeObservable(this, {
      sources: observable,
      parameters: observable,
      selectedNodeName: observable,
      addNewSource: action,
      setSelectedSource: action,
      setSelectedSourceParameter: action,
      deleteSource: action
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  handleSocketChange (socket) {
    this.nodeAPI = null

    if (socket) {
      this.nodeAPI = new NodeAPI(socket)

      this.nodeAPI.onConfirmationCreation(res =>
        this.handleSourceCreation(res)
      )
    }
  }

  /**
   * Handles the creation of a source
   * @param {object} res - The created source
   */
  handleSourceCreation (res) {
    if (!this.nodeAlreadyExist(res.name)) {
      this.addNewSource(
        Object.assign(new SourceNode(res.name, res.node.synthdefName)),
        res.node
      )
    }

    this.parameters.get(res.name)?.set('synthdefName', res.node.synthdefName)

    const proprietes = res.proprieties.reduce(function (acc, p) {
      acc.set(p[0], { type: p[1], value: p[2] })
      return acc
    }, new Map())

    this.parameters.get(res.name)?.set('properties', proprietes)
  }

  /**
   * Add the new source
   * @param {SourceNode} source - The new source
   */
  addNewSource (source) {
    if (this.parameters.has(source.nodeName)) {
      this.parameters.get(source.nodeName)?.set('synthdefName', source.synthdefName)
      this.sources[source.nodeName].synthdefName = source.synthdefName
    } else {
      this.sources.set(source.nodeName, source)
      this.parameters.set(source.nodeName, source.toMap())
    }

    this.nodeAPI.createSource(source)
  }

  /**
   * Add the new source
   * @param {SourceNode} source - The new selected source
   */
  setSelectedSource (sourceKey) {
    if (this.parameters.get(sourceKey)) {
      this.selectedNodeName = sourceKey
    }
  }

  setSelectedSourceParameter (param, value) {
    this.parameters.get(this.selectedNodeName)?.set(param, value)
    if (param === 'synthdefName') {
      this.nodeAPI.createSource(this.selectedSource)
    } else {
      this.nodeAPI.updateSource(this.selectedSource)
    }
  }

  setSelectedSourceSynthdefParameter (param, value) {
    const p = this.parameters.get(this.selectedNodeName)?.get('properties').get(param)
    p.value = value

    this.parameters.get(this.selectedNodeName)?.get('properties').set(param, p)
    this.nodeAPI.updateSynthdefSource({ nodeName: this.selectedNodeName, param: { propertie: param, value: value } })
  }

  deleteSource (source) {
    if (this.selectedNodeName === source) {
      this.selectedNodeName = null
    }
    if (this.parameters.get(source)) {
      this.parameters.delete(source)
    }
    delete this.sources[source]
  }
}

export default SourceStore
