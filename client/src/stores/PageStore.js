import { makeObservable, observable, action } from 'mobx'
import PageEnum from '@models/PageEnum'

/** Stores all pages */
class PageStore {
  /** @property {string} [activePage="settings"] - The currently active page */
  activePage = PageEnum.SETTINGS

  constructor () {
    makeObservable(this, {
      activePage: observable,
      setActivePage: action
    })
  }

  /**
   * Set the new active page
   * @param {string} activePage - The active page
   */
  setActivePage (activePage) {
    this.activePage = activePage
  }
}

export default PageStore
