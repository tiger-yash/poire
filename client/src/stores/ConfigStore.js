import { makeObservable, observable, action, reaction, computed, toJS } from 'mobx'

import Ajv from 'ajv'
import satieSchema from '@models/satie.schema.json'

import logger from '@utils/logger'
import SatieAPI from '@api/SatieAPI'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the ConfigStore
 * @private
 */
export const LOG = logger.child({ store: 'ConfigStore' })

/**
 * @constant {external:ajv} ajv - The JSON schema validator
 * @private
 */
const ajv = new Ajv()

/** @constant {object[]} AVAILABLE_LISTENING_FORMATS - All listening formats for SATIE */
export const AVAILABLE_LISTENING_FORMATS = [{
  label: 'Stereo Listener',
  value: 'stereoListener'
}, {
  label: 'Dome VBAP',
  value: 'domeVBAP'
}, {
  label: 'Quad Panner',
  value: 'quadpanaz'
}
]

/** Stores all SATIE configuration */
class ConfigStore {
  /** @property {api.SatieAPI} satieApi - Higher level API for SATIE */
  satieAPI = null

  /** @property {external:ajv} validateSatieConfig - Validator for the JSON configuration of SATIE */
  validateSatieConfig = ajv.compile(satieSchema)

  /** @property {Map<string, number|string|object|array>} satieConfig - The SATIE configuration */
  satieConfig = {
    server: {
      name: null,
      supernova: true
    },
    listeningFormat: ['stereoListener'],
    outBusIndex: [0],
    numAudioAux: 2,
    ambiOrders: [1],
    minOutputBusChannels: 2,
    userPluginsDir: null
  }

  /** @property {string} currentListeningFormat - The current listening format */
  get currentListeningFormat () {
    return AVAILABLE_LISTENING_FORMATS.find(format => {
      return format.value === this.satieConfig.listeningFormat[0]
    })
  }

  /** @property {number[]} outputChannels - All configured output channels */
  get outputChannels () {
    return [...Array(this.satieConfig.minOutputBusChannels).keys()]
  }

  constructor (socketStore) {
    makeObservable(this, {
      satieConfig: observable,
      setSatieConfig: action,
      currentListeningFormat: computed,
      outputChannels: computed
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  /**
   * Handles changes to the app's socket
   * @param {external:socketIO/Socket} socket - Event-driven socket
   */
  handleSocketChange (socket) {
    this.satieAPI = null

    if (socket) {
      this.satieAPI = new SatieAPI(socket)
    }
  }

  /**
   * Apply the SATIE configuration
   * @async
   */
  async applySatieConfiguration () {
    const currentConfig = toJS(this.satieConfig)

    try {
      await this.satieAPI.configure({
        satie_configuration: currentConfig
      })

      LOG.info({
        msg: 'SATIE configuration is applied',
        config: currentConfig
      })
    } catch (error) {
      LOG.info({
        msg: 'Failed to apply SATIE configuration',
        error: error
      })
    }
  }

  /**
   * Set the SATIE configuration
   * @todo Check the JSON schema only when the configuration is fetched
   * @param {object} json - The JSON based SATIE configuration
   */
  setSatieConfig (json) {
    if (this.validateSatieConfig(json)) {
      this.satieConfig = json

      LOG.info({
        msg: 'SATIE configuration is updated',
        config: json
      })
    } else {
      LOG.error({
        msg: 'SATIE configuration isn\'t valid',
        error: this.validateSatieConfig.errors
      })
    }
  }

  updateSatieConfig (json) {
    this.setSatieConfig({
      ...toJS(this.satieConfig),
      ...json
    })
  }
}

export default ConfigStore
