import { makeObservable, observable, action, reaction, computed, toJS } from 'mobx'

import logger from '@utils/logger'
import TestAPI from '@api/TestAPI'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the TesterStore
 * @private
 */
export const LOG = logger.child({ store: 'TesterStore' })

/**
 * @constant {object[]} AVAILABLE_TEST_SYNTH - All usable synths for the DAC tester
 * @todo Synchronize this list with the Poire Server / PySATIE
 */
export const AVAILABLE_TEST_SYNTH = [{
  label: 'White Noise',
  value: 'whitenoise'
}, {
  label: 'Sine',
  value: 'sine'
}]

/** Stores all tester configuration */
class TesterStore {
  /** @property {api.TestAPI} testAPI - API for SATIE testing */
  testAPI = null

  /** @property {Map<string, number|string|object|array>} dacConfig - The tester configuration */
  dacConfig = {
    testInterval: 500,
    frequency: 423,
    testSynth: 'whitenoise'
  }

  /** @property {?number} testingChannel - A channel that is currently tested by the DAC tester */
  testingChannel = null

  constructor (socketStore) {
    makeObservable(this, {
      dacConfig: observable,
      testingChannel: observable,
      setTesterConfig: action,
      setTestingChannel: observable,
      currentTestSynth: computed,
      hasExtraConfig: computed
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  /** @property {string} currentTestSynth - The current test synth */
  get currentTestSynth () {
    return AVAILABLE_TEST_SYNTH.find(synth => {
      return synth.value === this.dacConfig.testSynth
    })
  }

  /** @property {boolean} hasExtraConfig - Checks if the test synth requires extra arguments */
  get hasExtraConfig () {
    const { testSynth } = toJS(this.dacConfig)

    switch (testSynth) {
      case 'sine':
        return true
      default:
        return false
    }
  }

  /** @property {object} extraConfig - Gets the extra configuration of the selected synth */
  get extraConfig () {
    const { frequency, testSynth } = toJS(this.dacConfig)

    switch (testSynth) {
      case 'sine':
        return { frequency }
      default:
        return {}
    }
  }

  /**
   * Handles changes to the app's socket
   * @param {external:socketIO/Socket} socket - Event-driven socket
   */
  handleSocketChange (socket) {
    this.testAPI = null

    if (socket) {
      this.testAPI = new TestAPI(socket)
    }
  }

  /**
   * Starts a DAC test
   * @param {number} channelNum - DAC channel to test
   */
  async applyTestStart (channelNum) {
    const { testSynth } = toJS(this.dacConfig)

    try {
      this.setTestingChannel(channelNum)

      await this.testAPI.testDacChannel(
        channelNum,
        testSynth,
        this.extraConfig
      )
    } catch (error) {
      this.setTestingChannel(null)

      LOG.error({
        msg: 'Failed to start test',
        channel: channelNum,
        error: error.message
      })
    }
  }

  /**
   * Stops a DAC test
   * @param {number} channelNum - The tested DAC channel
   */
  async applyTestStop (channelNum) {
    try {
      await this.testAPI.stopDacChannel(channelNum)
      this.setTestingChannel(null)
    } catch (error) {
      LOG.error({
        msg: 'Failed to stop the test',
        channel: channelNum,
        error: error.message
      })
    }
  }

  /**
   * Applies a timeout on a started test
   * @param {number} channelNum - The tested DAC channel
   * @returns {Promise} A promise that is resolved when the timeout is triggered
   */
  applyTestTimeout (channelNum, testInterval) {
    return new Promise(resolve => {
      setTimeout(async () => {
        await this.applyTestStop(channelNum)
        resolve(channelNum)
      }, testInterval)
    })
  }

  /**
   * Tests a DAC channel during the configured test interval
   * @param {number} channelNum - DAC channel to test
   */
  async applyDacTest (channelNum) {
    const { testInterval } = toJS(this.dacConfig)

    await this.applyTestStart(channelNum)
    await this.applyTestTimeout(channelNum, testInterval)
  }

  /**
   * Generate a DAC test for each channel
   * @generator
   * @async
   * @yield {number} The tested channel
   * This runs all asynchronous tests sequencially
   * @see [Async iteration and generators]{https://javascript.info/async-iterators-generators}
   */
  async * generateDacTests (outputChannels) {
    for (const channelNum of outputChannels) {
      await this.applyDacTest(channelNum)
      yield channelNum
    }
  }

  /**
   * Cycle the test for each channel
   * @param {number[]} outputChannels - Every channels to test
   */
  async applyDacCycle (outputChannels) {
    for await (const testedChannel of this.generateDacTests(outputChannels)) {
      LOG.info({
        msg: 'A channel has been tested',
        channel: testedChannel
      })
    }
  }

  /**
   * Sets the currently tested channel
   * @param {?number} channelNum - The channel that is currently tested or null if no channel is tested
   */
  setTestingChannel (channelNum) {
    this.testingChannel = channelNum
  }

  /**
   * Sets the tester configuration
   * @param {object} json - The tester configuration
   */
  setTesterConfig (json) {
    this.dacConfig = json

    LOG.info({
      msg: 'Tester configuration is updated',
      config: json
    })
  }

  /**
   * Updates the tester config with a part of the whole config object
   * @param {object} json - Part of the whole config object
   */
  updateTesterConfig (json) {
    this.setTesterConfig({
      ...toJS(this.dacConfig),
      ...json
    })
  }
}

export default TesterStore
