import { makeObservable, observable, action, computed, reaction } from 'mobx'
import PluginAPI from '@api/PluginAPI'
import logger from '@utils/logger'

/**
 * @constant {external:pino/logger} LOG - Dedicated logger for the PluginStore
 * @private
 */
export const LOG = logger.child({ store: 'PluginStore' })

/** Stores all SATIE plugins */
class PluginStore {
  /** @property {module:api.PluginAPI} - API that introspects all plugins */
  pluginAPI = null

  /** @property {object[]} - All available plugins */
  pluginList = []

  /** @property {boolean} - Flag stored plugins */
  get hasPlugins () {
    return this.pluginList.length > 0
  }

  /** @property {string} - Get the default synth label */
  get defaultSynth () {
    return this.hasPlugins ? this.pluginList[0].value : ''
  }

  constructor (socketStore) {
    makeObservable(this, {
      pluginList: observable,
      defaultSynth: computed,
      setPluginList: action
    })

    reaction(
      () => socketStore.activeSocket,
      socket => this.handleSocketChange(socket)
    )
  }

  /**
   * Handles changes to the app's socket
   * @param {external:socketIO/Socket} socket - Event-driven socket
   */
  handleSocketChange (socket) {
    this.pluginAPI = null

    if (socket) {
      this.pluginAPI = new PluginAPI(socket)

      this.pluginAPI.onPluginUpdate(res =>
        this.handlePluginUpdate(res)
      )
    }
  }

  /**
   * Handles the update of all plugins
   * @param {object[]} ressources - All SATIE plugins
   */
  handlePluginUpdate (ressources) {
    const plugins = ressources.reduce((acc, plugin) => {
      acc.push({ label: plugin, value: plugin })
      return acc
    }, [])

    this.setPluginList(plugins)
  }

  /** Queries the SATIE plugins */
  applyAudioSourceQuery () {
    if (this.pluginAPI) {
      this.pluginAPI.queryAudioSources()
    }
  }

  /**
   * Sets the list of all plugins
   * @param {object[]} pluginList - All current plugins
   */
  setPluginList (pluginList) {
    this.pluginList = pluginList

    LOG.info({
      msg: 'The plugin list is initialized',
      plugins: pluginList
    })
  }
}

export default PluginStore
