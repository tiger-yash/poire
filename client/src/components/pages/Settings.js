import React, { useContext } from 'react'
import { AppStoresContext } from '@components/App'

import {
  ServerNameInput,
  SupernovaSwitch,
  ListeningFormatSelect,
  OutBusIndexInput,
  NumAudioAuxInput,
  AmbiOrdersInput,
  MinOutputBusChannelsInput
} from '@components/fields/SatieConfigFields'

import '@styles/pages/Settings.scss'

import { Common } from '@sat-valorisation/ui-components'

const { Button } = Common

/**
 * The Settings page
 * The settings define all SATIE configuration into block of fields as forms
 * @see [Definition of ARIA roles]{@link https://www.w3.org/TR/wai-aria-1.1/#role_definitions}
 * @selector `#Settings`
 * @memberof module:components/pages
 * @returns {external:react/Component} A page
 */
function Settings () {
  const { configStore } = useContext(AppStoresContext)

  return (
    <div id='Settings'>
      <header>
        <h1>
          Settings
        </h1>
      </header>
      <main>
        <section role='form' id='SuperColliderOptions'>
          <h2>
            SuperCollider Options
          </h2>
          <ServerNameInput />
          <SupernovaSwitch />
        </section>
        <section role='form' id='SatieOptions'>
          <h2>
            SATIE Options
          </h2>
          <ListeningFormatSelect />
          <OutBusIndexInput />
          <AmbiOrdersInput />
          <NumAudioAuxInput />
          <MinOutputBusChannelsInput />
        </section>
      </main>
      <footer>
        <Button onClick={() => configStore.applySatieConfiguration()}>
          Configure
        </Button>
      </footer>
    </div>
  )
}

export default Settings
