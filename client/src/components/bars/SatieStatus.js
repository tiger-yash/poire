import React from 'react'
import classNames from 'classnames'

import Expand from '@assets/svg/expand_24px.svg'
import { useStore } from '@stores'

import '@styles/bars/SatieStatus.scss'

/**
 * Display a status given by the Satie server
 * @selector `.StatusInfo`
 * @memberof module:components/bars.SatieStatus
 * @param {string} label - Label of the status
 * @param {string|number} value - Number of the status
 * @param {boolean} connected - Checks if the Satie server is connected
 * @returns {external:react/Component} An info line from the Satie statuses
 */
function StatusInfo ({ label, value, connected = true }) {
  const statusClass = classNames(connected ? 'text--green' : 'text--regular')

  return (
    <div className='StatusInfo'>
      <div className='text--regular'>{label}</div>
      <div className={statusClass}>{value}</div>
    </div>
  )
}

/**
 * All Satie statuses
 * @selector `#SatieStatus`
 * @memberof module:components/bars
 * @returns {external:react/Component} A status panel
 */
function SatieStatus () {
  const { state } = useStore()
  const { connected } = state.satieConfig
  return (
    <div id='SatieStatus'>
      {!connected && (
        <StatusInfo label='Status:' value='Not connected' connected={false} />
      )}
      {connected && (
        <>
          <img
            className='expand-icon'
            src={Expand}
            alt='expand'
            height='24px'
            width='24px'
          />
          <StatusInfo label='Status:' value='Connected' />
          <StatusInfo label='IP:' value='127.0.0.1' />
          <StatusInfo label='Port:' value='57110' />
          <StatusInfo label='SC Server:' value='Supernova' />
          <StatusInfo label='No. Out Chan.:' value='4' />
          <StatusInfo label='List. Format:' value='stereoListener' />
          <StatusInfo label='AmbiOrders' value='None' />
        </>
      )}
    </div>
  )
}

export default SatieStatus
