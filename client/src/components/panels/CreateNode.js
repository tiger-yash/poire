import React, { useState, useEffect, useContext } from 'react'
import { observer } from 'mobx-react-lite'

import { AppStoresContext } from '@components/App'
import SourceNode from '@models/SourceNode'

import {
  SourceNodeNameInput,
  SynthDefSelect
} from '@components/fields/PlaygroundFields'

/**
 * The header of the playground. It submits the creation of a source.
 * @fires A query event which populates a dropdown menu with a selection of sources available.
 * @todo It should take in as props a function to add a source to the nodeList.
 * @selector `.CreateNode`
 * @memberof module:components/panels
 * @returns {external:react/Component} An input parameter + a button
 */
const CreateNode = observer(() => {
  const { sourceStore, pluginStore } = useContext(AppStoresContext)

  const [nodeNameStatus, setnodeNameStatus] = useState('')
  const [error, setError] = useState('')
  const [nodeName, setNodeName] = useState('')
  const [selectedSynth, selectSynth] = useState(pluginStore.defaultSynth)

  useEffect(() => {
    pluginStore.applyAudioSourceQuery()
  }, [])

  useEffect(() => {
    selectSynth(pluginStore.defaultSynth)
  }, [pluginStore.hasPlugins])

  const handleChange = (e) => {
    setNodeName(e.value)
    if (sourceStore.nodeAlreadyExist(e.value)) {
      setnodeNameStatus('busy')
      setError('Warning: Node already exist')
    } else {
      setnodeNameStatus('')
      setError('')
    }
  }

  const handleSelect = (name) => {
    const setSelected = (selectedItems) => {
      selectSynth(selectedItems.value)
    }
    return setSelected
  }

  const handleCreateNode = () => {
    if (nodeName !== '') {
      const newSource = new SourceNode(nodeName, selectedSynth)
      sourceStore.addNewSource(newSource)
      setNodeName('')
      selectSynth(pluginStore.defaultSynth)
    } else {
      setnodeNameStatus('danger')
      setError('Error: you need to fill this field')
    }
  }

  return (
    <div className='CreateNode'>
      <h2>
        Create Source Node
      </h2>
      <SourceNodeNameInput
        isDisabled={false}
        value={nodeName}
        setValue={handleChange}
        status={nodeNameStatus}
        description={error}
      />
      <SynthDefSelect
        value={selectedSynth}
        setValue={handleSelect('synthdefName')}
        isDisabled={false}
        pluginList={pluginStore.pluginList}
      />

      <button type='button' className='text-button' onClick={() => handleCreateNode()}>
        Create Node
      </button>
    </div>
  )
})

export default CreateNode
