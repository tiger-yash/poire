import pino from 'pino'

/**
 * Gets the level of the logger according to the app context
 * @memberof module:utils/logger
 * @returns {external:pino/level} The logger level
 */
function getLoggerLevel () {
  let level = 'info'

  const currentURL = new URL(document.location)
  const urlParameters = currentURL.searchParams
  const urlLevel = urlParameters.get('level')

  if (urlLevel) {
    level = urlLevel
  }

  return level
}

/**
 * Creates a singleton logger for the webapp
 * @memberof module:utils/logger
 * @member {external:pino/logger} logger
 */
const logger = pino({
  level: getLoggerLevel(),
  browser: {
    asObject: true
  }
})

export default logger
