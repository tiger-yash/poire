import React from 'react'
import Settings from '../components/Settings/index'

export default {
  title: 'Settings',
  component: Settings
}

export const SettingsDefault = () => <Settings />
