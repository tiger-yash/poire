## Folder Structure

```
client/  
├── build/ 
├── public/
├── src/ (this is where the app lives, any JS and CSS **has** to be inside this folder, otherwise will be ignored by webpack)   
|     ├── assets/ 
|     ├── components/  
|     |   ├── __tests__/
|     |   ├── common/  
|     |   |   ├── index.js 
|     |   |   ├── Parameter.js
|     |   |   └── Parameter.scss (and others)
|     |   ├── Component/ (Sidebar, Playground etc. )
|     |   |   ├── index.js
|     |   |   ├── Subcomponent.js (and others)
|     |   |   └── styles.scss (stylesheet for the whole component)
|     |   ├── index.js (default exports of components for use in App.js)
|     ├── stories/
|     ├── styles/
|     ├── App.js
|     ├── App.scss
|     ├── App.test.js
|     ├── index.js
|     ├── index.scss
|     ├── serviceWorker.js
|     ├── setupTests.js
|     ├── store.js
|     └── test-utils.js
```

## About:

- The build folder is generated when running `npm run build`. The server is configured to use the generated build.
- The public folder contains the html template, this file **must** be there and be named `index.html`. See [React Docs](https://create-react-app.dev/docs/folder-structure/)
- The javascript entry point is `src/index.js`, this file is also required to exist and have that name.
- the `stories` directory is for [storybook](https://storybook.js.org/), a tool for developing components in isolation.
- the `styles` directory is for any shared SASS files, variables, mixins, etc.

## store.js
This is where the global state of the application lives, as well as the reducer and dispatch function (used to modify the state). Information that needs to be shared between several components should live here. The socket is also initialized here, and some event listeners are registered. 



### Testing

To run the tests use ```npm run test```

You can also generate a coverage report using ```npm run test -- --coverage --watchAll=false```

See the documentation for more information:

- [testing-library](https://testing-library.com/docs/react-testing-library/intro)
- [jest](https://jestjs.io/)


