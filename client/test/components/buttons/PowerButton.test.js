/* global jest beforeEach afterEach describe it expect */

import { cleanup, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'

import React from 'react'

import populateStores from '~/src/populateStores.js'

import { renderWithContext } from '~/test/utils/testUtils'
import PowerButton from '@components/buttons/PowerButton'

afterEach(cleanup)

describe('<PowerButton />', () => {
  let stores

  beforeEach(() => {
    stores = populateStores()

    stores.statusStore.applyInitialization = jest.fn()
    stores.statusStore.applyDisconnection = jest.fn()
  })

  it('render without crashing', () => {
    renderWithContext(<PowerButton />)
  })

  describe('disconnected', () => {
    it('should suggest booting SATIE when it is not connected', () => {
      renderWithContext(<PowerButton />, { stores })
      expect(screen.getByRole('button')).toHaveTextContent('BOOT SATIE')
    })

    it('should boot SATIE when it is clicked', () => {
      renderWithContext(<PowerButton />, { stores })
      userEvent.click(screen.getByRole('button'))
      expect(stores.statusStore.applyInitialization).toHaveBeenCalled()
      expect(stores.statusStore.applyDisconnection).not.toHaveBeenCalled()
    })
  })

  describe('connected', () => {
    beforeEach(() => {
      stores.statusStore.setConnectionStatus(true)
    })

    it('should suggest stopping SATIE when it is connected', () => {
      renderWithContext(<PowerButton />, { stores })
      expect(screen.getByRole('button')).toHaveTextContent('STOP SATIE')
    })

    it('should disconnect SATIE when it is clicked', () => {
      renderWithContext(<PowerButton />, { stores })
      userEvent.click(screen.getByRole('button'))
      expect(stores.statusStore.applyInitialization).not.toHaveBeenCalled()
      expect(stores.statusStore.applyDisconnection).toHaveBeenCalled()
    })
  })
})
