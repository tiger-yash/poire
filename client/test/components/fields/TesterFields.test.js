/* global jest beforeEach afterEach describe it expect */

import { cleanup, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'
import userEvent from '@testing-library/user-event'
import { renderWithContext } from '~/test/utils/testUtils'

import React from 'react'

import populateStores from '~/src/populateStores.js'
import { AVAILABLE_TEST_SYNTH } from '@stores/TesterStore'

import {
  TestSynthSelect,
  TestIntervalInput,
  FrequencyInput
} from '@components/fields/TesterFields'

afterEach(cleanup)

let stores

beforeEach(() => {
  stores = populateStores()
  stores.testerStore.updateTesterConfig = jest.fn()
})

describe('<TestSynthSelect />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<TestSynthSelect />, { stores })
    expect(screen.getByText(/Test Synth/i)).toBeInTheDocument()
  })

  it('should select the configured format by default', () => {
    renderWithContext(<TestSynthSelect />, { stores })
    expect(screen.getByText(stores.testerStore.currentTestSynth.label)).toBeInTheDocument()
  })

  it('should update the configuration when it is clicked', () => {
    renderWithContext(<TestSynthSelect />, { stores })
    const sineOpt = AVAILABLE_TEST_SYNTH.find(opt => opt.value === 'sine')

    userEvent.click(screen.getByRole('button'))
    userEvent.click(screen.getByText(sineOpt.label))

    expect(stores.testerStore.updateTesterConfig).toHaveBeenCalledWith({
      testSynth: sineOpt.value
    })
  })
})

describe('<TestIntervalInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<TestIntervalInput />, { stores })
    expect(screen.getByText('Test interval')).toBeInTheDocument()
  })

  it('should increment the configured interval when the increment button is clicked', () => {
    const { container } = renderWithContext(<TestIntervalInput />, { stores })
    userEvent.click(container.querySelector('.InputButtonIncrement > button'))

    expect(stores.testerStore.updateTesterConfig).toHaveBeenCalledWith({
      testInterval: stores.testerStore.dacConfig.testInterval + 100
    })
  })

  it('should decrement the configured interval when the decrement button is clicked', () => {
    const { container } = renderWithContext(<TestIntervalInput />, { stores })
    userEvent.click(container.querySelector('.InputButtonDecrement > button'))

    expect(stores.testerStore.updateTesterConfig).toHaveBeenCalledWith({
      testInterval: stores.testerStore.dacConfig.testInterval - 100
    })
  })
})

describe('<FrequencyInput />', () => {
  it('should render the field with its title', () => {
    renderWithContext(<FrequencyInput />, { stores })
    expect(screen.getByText('Signal frequency')).toBeInTheDocument()
  })

  it('should increment the configured interval when the increment button is clicked', () => {
    const { container } = renderWithContext(<FrequencyInput />, { stores })
    userEvent.click(container.querySelector('.InputButtonIncrement > button'))

    expect(stores.testerStore.updateTesterConfig).toHaveBeenCalledWith({
      frequency: stores.testerStore.dacConfig.frequency + 100
    })
  })

  it('should decrement the configured interval when the decrement button is clicked', () => {
    const { container } = renderWithContext(<FrequencyInput />, { stores })
    userEvent.click(container.querySelector('.InputButtonDecrement > button'))

    expect(stores.testerStore.updateTesterConfig).toHaveBeenCalledWith({
      frequency: stores.testerStore.dacConfig.frequency - 100
    })
  })
})
