/* global afterEach describe it expect */

import { cleanup, screen } from '@testing-library/react'
import '@testing-library/jest-dom/extend-expect'

import React from 'react'

import { renderWithContext } from '~/test/utils/testUtils'
import Settings from '@components/pages/Settings'

afterEach(cleanup)

describe('<Settings />', () => {
  it('should render 2 forms of fields', () => {
    renderWithContext(<Settings />)
    expect(screen.getAllByRole('form').length).toEqual(2)
    expect(screen.getByText(/SuperCollider Options/i)).toBeInTheDocument()
    expect(screen.getByText(/SATIE Options/i)).toBeInTheDocument()
  })

  it('should render all configuration fields', () => {
    const { container } = renderWithContext(<Settings />)
    expect(container.querySelectorAll('.Field').length).toEqual(7)
  })
})
